import { writable } from 'svelte/store';

// Create a writable store to track the scroll position
export const scrollPosition = writable(Window);
